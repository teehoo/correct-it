# Correct-it

## Niveau d'accès
 L'application possède 4 niveaux d'accès : 
 * guest
 * élève
 * professeur
 * administrateur
 
Pour chacun des ces niveaux (sauf le guest), on va créer 3 tables différentes.


## Notes Auth

* Les guards => définissent comment les utilisateurs sont autentifiés pour chaque requête.
* Les providers indiques à Laravel qu'est ce qu'il doit utiliser pour authentifier et valider quand on essaye d'utiliser les guards.

## Notes importantes
* Pendant la phase de dev, il faut désactiver le middleware VerifyCsrfToken car il empeche le débuggage avec Postman. Ne pas oublier de le remettre en prod !
* Pour le scss suivre l'architecture suivante : https://gist.github.com/rveitch/84cea9650092119527bc
* Lors de la récupération du project : composer update & npm install et copier le .env.exemple en .env et remplir les informations pour la connection à la base local
* Pour compiler le scss : npm run watch

## Fonctionnement du Javascript
Le point d'entrée de l'execution des scripts JS est géré par le router mis en place.
Les scripts sont ainsi exécutés en fonction de la page que l'utilisateur visite.
Il est donc important de respecter ces 3 points :

* Quand on créé une view Blade, il faut renseigner un namespace :
`@section('namespace', 'home')`

* Ensuite, on vient mapper les script à executer (un PageModule) avec ce namespace dans le fichier `www/resources/js/route/route.js`

* Puis on créé un PageModule qui doit être une classe héritant de `www/resources/js/route/pages/BasePageModule.js`
Ce dernier doit respecter une architecture minimum pour fonctionner. Exemple avec le HomeModulePage :
```
export default class HomePageModule extends BasePageModule{
    constructor(request) {
        super(request);
        this.initElements();
        this.initEvents();
        this.fire();
    }

    initElements() {

    }

    initEvents() {

    }

    fire() {

    }
}
```

Ici on passe dans le constructeur `request` et il est important de le fournir au constructeur parent `super(request);`.
Ceci est le strict minimum pour que le PageModule fonctionne. Il est tout à fait possible de rajouter des méthodes, des propriétés de classes ... si besoin.

## Les Seeders
Pour éviter de peupler à la main notre BDD, des seeders on été mis en place pour la peupler dynamiquement.
Pour se faire il suffit de :
* Refresh sa BDD existante :
``php artisan migrate:fresh``
* Demarrer son serveur : `php artisan serve`
* Dans le navigateur, il faut saisir les urls suivantes :
* Pour créer les deux admins : ``${APP_ROUTE}:8000/seeder/admins``
* Pour créer 100 students : ``${APP_ROUTE}:8000/seeder/students``
* Pour créer 100 teachers : ``${APP_ROUTE}:8000/seeder/teachers``

## Les tests end to end
Pour s'assurer que les nouvelles fonctionnalités n'affectent pas les anciennes il est nécessaire de lancer les tests end to end via la commande suivante

``.\node_modules\.bin\cypress open``

Une fois le panel cypress lancé, il suffit juste de lancer les tests manuellement