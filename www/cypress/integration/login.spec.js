import { HOST } from '../support/constants';
import { admin } from '../fixtures/users';

describe('Login', () => {
    /*STUDENT LOGIN*/
    /*TODO*/

    /*TEACHER LOGIN*/
    /*TODO*/

    /*ADMIN LOGIN*/
    /*FIXME*/
    /*This test need to be fixed because of a css property*/
    it('admin successfully login', () => {
        cy.visit(HOST + '/student/login')
        cy.get('#email').type(admin.email)
        cy.get('#password').type(admin.password)
        cy.get('button').contains("Se connecter").click()
        cy.get('.link-container').should('contain', name)
    })

})
