import faker from 'faker'
import { HOST } from '../support/constants';
import { register } from '../support/functions'

describe('Registration', () => {

    /*STUDENT REGISTRATION*/
    it('student successfully registered', () => {
        cy.visit(HOST + '/student/register')
        register(false)
    })

    /*TEACHER REGISTRATION*/
    it('teacher successfully registered', () => {
        cy.visit(HOST + '/teacher/register')
        register(true)
    })

})
