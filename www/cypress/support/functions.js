import faker from "faker";

export function register(isTeacher) {
    const email = faker.internet.email()
    const password = faker.internet.password()
    const name = faker.name.findName()

    cy.get('input[name=name]').type(name)
    cy.get('input[name=email]').type(email)
    cy.get('input[name=password]').type(password)
    cy.get('input[name=password_confirmation]').type(password)

    if(isTeacher) {
        const fixture_id_card = 'fake_id_card.png';
        cy.get('.drop-files__fake').attachFile(fixture_id_card);
        cy.wait(2000);
        cy.get('button').contains("S'enregistrer").click()
    } else {
        cy.get('button').contains("S'inscrire").click()
    }

    cy.get('.link-container').should('contain', name)
}
