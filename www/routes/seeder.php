<?php

use Illuminate\Support\Facades\Route;

Route::prefix('seeder')->namespace('Seeder')->group(function () {
    // All
    Route::get('all', 'SeederController@seedAll');
    // Admins
    Route::get('admins', 'SeederController@seedAdmin');
    // Teachers
    Route::get('teachers', 'SeederController@seedTeacher');
    // Students
    Route::get('students', 'SeederController@seedStudent');
    // Categories
    Route::get('categories', 'SeederController@seedCategory');
    // Courses
    Route::get('courses', 'SeederController@seedCourse');
});
