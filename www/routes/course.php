<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Course
|--------------------------------------------------------------------------
*/

Route::namespace('Course')->group(function () {
    Route::prefix('/course')->group(function () {
        // Show all courses
        Route::get('/', 'CourseController@all')->name('course.all');

        // Search a course
        Route::post('/', 'CourseController@search')->name('course.search');

        // Show a course
        Route::get('/{id}', 'CourseController@show')->name('course.show');

        // Show edit course page
        Route::get('/edit/{id}', 'CourseController@edit')
            ->middleware('auth:teacher,admin')
            ->name('course.edit');

        // Show edit course page
        Route::post('/edit/{id}', 'CourseController@update')
            ->middleware('auth:teacher,admin')
            ->name('course.update');

        // Delete a course
        Route::get('/delete/{id}', 'CourseController@delete')
            ->middleware('auth:teacher,admin')
            ->name('course.delete');
    });
});
