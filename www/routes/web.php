<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

require_once (__DIR__ . '/auth.php');
require_once (__DIR__ . '/admin.php');
require_once (__DIR__ . '/teacher.php');
require_once (__DIR__ . '/student.php');
require_once (__DIR__ . '/course.php');

// Only provided if we are in dev environement
if(App::environment('local')) {
    require_once (__DIR__ . '/tests.php');
    require_once (__DIR__ . '/seeder.php');
}

/*
|--------------------------------------------------------------------------
| Common routes
|--------------------------------------------------------------------------
*/

Route::get('/', 'HomeController@Index')->name('home');
