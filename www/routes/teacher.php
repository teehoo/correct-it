<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Teacher
|--------------------------------------------------------------------------
*/
Route::namespace('Teacher')->group(function () {
    // Space
    Route::get('/teacher/space', 'TeacherSpaceController@index')
        ->middleware('guest')
        ->name('teacher.space');

    // Dashboard
    Route::prefix('/teacher/dashboard')->middleware(['auth:teacher', 'teacher.validated'])->group(function () {
        //-- General
        Route::get('/', 'TeacherDashboardController@index')->name('teacher.dashboard');
        //-- Course creation
        Route::get('/course/create', 'TeacherDashboardController@indexCourseCreate')->name('teacher.dashboard.course.create');
        Route::post('/course/create', 'TeacherDashboardController@createCourse')->name('teacher.dashboard.course.create');
        //-- Courses list
        Route::get('/course/list', 'TeacherDashboardController@indexCourseList')->name('teacher.dashboard.course.list');
    });
});
