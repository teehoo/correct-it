<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Auth routes
|--------------------------------------------------------------------------
*/

// ADMIN
Route::prefix('admin')->namespace('Auth\Admin')->group(function () {
Route::get('/login', 'AdminLoginController@index')->name('admin.login');
Route::post('/login', 'AdminLoginController@login')->name('admin.login');
Route::get('/register', 'AdminRegisterController@index')->name('admin.register');
Route::post('/register', 'AdminRegisterController@register')->name('admin.register');
});

// TEACHER
Route::prefix('teacher')->namespace('Auth\Teacher')->group(function () {
Route::get('/login', 'TeacherLoginController@index')->name('teacher.login');
Route::post('/login', 'TeacherLoginController@login')->name('teacher.login');
Route::get('/register', 'TeacherRegisterController@index')->name('teacher.register');
Route::post('/register', 'TeacherRegisterController@register')->name('teacher.register');
});

// STUDENT
Route::prefix('student')->namespace('\App\Http\Controllers\Auth\Student')->group(function () {
Route::get('/login', 'StudentLoginController@index')->name('student.login');
Route::post('/login', 'StudentLoginController@login')->name('student.login');
Route::get('/register', 'StudentRegisterController@index')->name('student.register');
Route::post('/register', 'StudentRegisterController@register')->name('student.register');
});

// LOGOUT
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
