<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin routes
|--------------------------------------------------------------------------
*/

Route::prefix('admin')->namespace('Admin')->group(function () {
    // Dashboard
    Route::prefix('/dashboard')->middleware('auth:admin')->group(function () {
        //-- general
        Route::get('', 'AdminDashboardController@index')->name('admin.dashboard');
        //-- teacher manager
        Route::get('/teacher', 'AdminDashboardController@indexTeacher')->name('admin.dashboard.teacher');
        //-- student manager
        Route::get('/student', 'AdminDashboardController@indexStudent')->name('admin.dashboard.student');
        //-- student manager
        Route::get('/course', 'AdminDashboardController@indexCourse')->name('admin.dashboard.course');
        //-- teacher account validation
        Route::get('/teacher/validation/{id}', 'AdminDashboardController@teacherValidation')
            ->name('admin.dashboard.teacher.validation');
    });
});
