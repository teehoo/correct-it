<?php

namespace App\View\Components\Form;

use App\Keys\Key;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ViewErrorBag;
use Illuminate\View\Component;

class FormGroup extends Component
{
    /**
     * @var ViewErrorBag
     */
    public $errors;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $type;
    /**
     * @var string
     */
    public $id;
    /**
     * @var bool
     */
    public $required;

    /**
     * Create a new component instance.
     *
     * @param string       $name
     * @param string       $type
     * @param string|null  $id
     * @param bool         $required
     */
    public function __construct(string $name, string $type, string $id = null, bool $required=false)
    {
        $errors = Session::get(Key::$errorsSession);
        $this->errors = is_null($errors) ? new ViewErrorBag() : $errors;
        $this->name = $name;
        $this->type = $type;
        $this->id = is_null($id) ? $name : $id;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.form.form-group');
    }
}
