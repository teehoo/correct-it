<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Teacher extends Authenticatable
{
    protected $guard = 'teacher';

    protected $fillable = [
        'name', 'email', 'password', 'validated', 'quart'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return mixed
     */
    public function toggleActivation()
    {
        $this->validated = !$this->validated;

        return $this;
    }

}
