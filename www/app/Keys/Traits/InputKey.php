<?php


namespace App\Keys\Traits;


trait InputKey
{
    // InputKey
    public static $inputName = 'name';
    public static $inputQuart = 'quart';
    public static $inputPassword = 'password';
    public static $inputPasswordConfirmation = 'password_confirmation';
    public static $inputEmail = 'email';
}
