<?php


namespace App\Keys;


use App\Keys\Traits\InputKey;
use App\Keys\Traits\SessionKey;

class Key
{
    use InputKey;
    use SessionKey;
}
