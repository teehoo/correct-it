<?php


namespace App\Repositories;



use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class Repository
 * @package App\Repositories
 */
abstract class Repository extends BaseRepository
{

}
