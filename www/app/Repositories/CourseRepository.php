<?php


namespace App\Repositories;


use App\Course;

class CourseRepository extends Repository
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Course::class;
    }

    public function onlyPublic()
    {
        return $this->where('public', true);
    }
}
