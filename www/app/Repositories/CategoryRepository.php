<?php


namespace App\Repositories;


use App\Category;

class CategoryRepository extends Repository
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Category::class;
    }
}
