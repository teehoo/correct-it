<?php


namespace App\Repositories;


use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;

interface RepositoryInterface
{
    /**
     * Return record by id
     *
     * @param string|integer $id
     * @return Model|null
     */
    public function find($id): ?Model;

    /**
     * Return a record or throw Exception if not found
     *
     * @param string|integer $id
     * @return Model
     *
     * @throws ModelNotFoundException
     */
    public function findOrFail($id): Model;

    /**
     * Create a record
     *
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model;

    /**
     * Return all records
     *
     * @return Collection|LengthAwarePaginator
     */
    public function all();

    /**
     * Set relations
     *
     * @param array $relations
     * @return $this
     */
    public function with(array $relations): self;

    /**
     * Return model of repository
     *
     * @return Model
     */
    public function getModel(): Model;

}
