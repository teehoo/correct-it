<?php


namespace App\Repositories\Traits;


trait Paginable
{
    protected $paginationValue = null;

    /**
     * Enable pagination and set a pagination value
     *
     * @param int $paginationValue
     *
     * @return $this
     */
    public function withPagination($paginationValue)
    {
        $this->paginationValue = $paginationValue;
        return $this;
    }

}
