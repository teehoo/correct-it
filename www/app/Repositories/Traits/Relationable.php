<?php


namespace App\Repositories\Traits;


trait Relationable
{
    protected $relations = [];

    /**
     * @param array $relations
     * @return void
     */
    public function setRelation(array $relations)
    {
        $this->relations = $relations;
    }
}
