<?php


namespace App\Repositories;


use App\Student;
use Illuminate\Database\Eloquent\Model;

class StudentRepository extends Repository
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Student::class;
    }
}
