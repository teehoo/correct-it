<?php


namespace App\Repositories;


use App\Admin;
use Illuminate\Database\Eloquent\Model;

class AdminRepository extends Repository
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Admin::class;
    }
}
