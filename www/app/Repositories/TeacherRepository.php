<?php


namespace App\Repositories;


use App\Teacher;

class TeacherRepository extends Repository
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return Teacher::class;
    }
}
