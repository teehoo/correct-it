<?php

namespace App\Http\Controllers\Auth\Teacher;

use App\Http\Controllers\Auth\LoginController;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeacherLoginController extends LoginController
{
    protected function index(Request $request)
    {
        return view('teacher.auth.login');
    }

    protected function guard()
    {
        return Auth::guard('teacher');
    }

    public function redirectPath()
    {
        return RouteServiceProvider::HOME;
    }
}
