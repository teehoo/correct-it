<?php

namespace App\Http\Controllers\Auth\Teacher;

use App\Http\Controllers\Auth\RegisterController;
use App\Keys\Key;
use App\Repositories\TeacherRepository;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TeacherRegisterController extends RegisterController
{
    private $teacherRepository;

    public function __construct(TeacherRepository $teacherRepository)
    {
        parent::__construct();
        $this->teacherRepository = $teacherRepository;
    }

    protected function index(Request $request)
    {
        return view('teacher.auth.register');
    }

    protected function create(array $data)
    {
        if (key_exists('password', $data)) {
            $data['password'] = Hash::make($data['password']);
        }

        /* Register the image of the quarte */

        /** @var UploadedFile $image */
        $image = $data['quart'];
        $nameImage = time() . '_' . $image->getClientOriginalName();
        $image = $image->move(public_path() . '/images/teachers/quarte/', $nameImage);
        $data['quart'] = $nameImage;

        return $this->teacherRepository->create($data);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            Key::$inputName => ['required', 'string', 'max:255'],
            Key::$inputEmail => ['required', 'string', 'email', 'max:255', 'unique:teachers'],
            Key::$inputPassword => ['required', 'string', 'min:8', 'confirmed'],
            Key::$inputQuart => ['required', 'mimes:jpeg,png,pdf']
        ]);
    }

    protected function guard()
    {
        return Auth::guard('teacher');
    }

    public function redirectPath()
    {
        return '/teacher/login';
    }
}
