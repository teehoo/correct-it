<?php

namespace App\Http\Controllers\Auth\Student;

use App\Http\Controllers\Auth\RegisterController;
use App\Keys\Key;
use App\Providers\RouteServiceProvider;
use App\Repositories\StudentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class StudentRegisterController extends RegisterController
{
    private $studentRepository;

    public function __construct(StudentRepository $studentRepository)
    {
        parent::__construct();
        $this->studentRepository = $studentRepository;
    }

    protected function index(Request $request)
    {
        return view('student.auth.register');
    }

    protected function create(array $data)
    {
        if (key_exists('password', $data)) {
            $data['password'] = Hash::make($data['password']);
        }

        return $this->studentRepository->create($data);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            Key::$inputName => ['required', 'string', 'max:255'],
            Key::$inputEmail => ['required', 'string', 'email', 'max:255', 'unique:students'],
            Key::$inputPassword => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    protected function guard()
    {
        return Auth::guard('student');
    }

    public function redirectPath()
    {
        return RouteServiceProvider::HOME;
    }


}
