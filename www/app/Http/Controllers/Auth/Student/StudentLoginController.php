<?php

namespace App\Http\Controllers\Auth\Student;

use App\Http\Controllers\Auth\LoginController;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentLoginController extends LoginController
{
    protected function index(Request $request)
    {
        return view('student.auth.login');
    }

    protected function guard()
    {
        return Auth::guard('student');
    }

    public function redirectPath()
    {
        return RouteServiceProvider::HOME;
    }
}
