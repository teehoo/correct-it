<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Auth\RegisterController;
use App\Keys\Key;
use App\Repositories\AdminRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Exceptions\ValidatorException;

class AdminRegisterController extends RegisterController
{
    private $adminRepository;

    public function __construct(AdminRepository $adminRepository)
    {
        parent::__construct();
        $this->adminRepository = $adminRepository;
    }

    protected function index(Request $request)
    {
        return view('admin.auth.register');
    }

    /**
     * @param array $data
     *
     * @return \App\User|mixed
     * @throws ValidatorException
     */
    protected function create(array $data)
    {
        if (key_exists('password', $data)) {
            $data['password'] = Hash::make($data['password']);
        }

        return $this->adminRepository->create($data);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            Key::$inputName => ['required', 'string', 'max:255'],
            Key::$inputEmail => ['required', 'string', 'email', 'max:255', 'unique:admins'],
            Key::$inputPassword => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }
}
