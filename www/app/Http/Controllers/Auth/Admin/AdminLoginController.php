<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends LoginController
{
    protected function index() {
        return view('admin.auth.login');
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function redirectPath()
    {
        return 'admin/dashboard';
    }
}
