<?php

namespace App\Http\Controllers\Seeder;

use App\Admin;
use App\Category;
use App\Course;
use App\Http\Controllers\Controller;
use App\Repositories\AdminRepository;
use App\Student;
use App\Teacher;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SeederController extends Controller
{
    private $_faker;

    public function __construct(Generator $faker)
    {
        $this->_faker = $faker;
    }

    const SEEDER_PASSWORD = 'secretsecret';

    /**
     * Run all seeders
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function seedAll()
    {
        $this->seedAdmin(true);
        $this->seedTeacher(true);
        $this->seedStudent(true);
        $this->seedCategory(true);
        $this->seedCourse(true);

        return $this->successResponse('All seeders created successfully');
    }

    /**
     * Create 4 admin users
     *
     * @param bool $returnBool
     *
     * @return \Illuminate\Http\JsonResponse|bool
     */
    public function seedAdmin($returnBool = false)
    {
        $seederData = [
            [
                'name' => 'theo',
                'email' => 'theo@admin.com',
                'password' => Hash::make(self::SEEDER_PASSWORD)
            ],
            [
                'name' => 'steven',
                'email' => 'steven@admin.com',
                'password' => Hash::make(self::SEEDER_PASSWORD)
            ],
            [
                'name' => 'george',
                'email' => 'george@admin.com',
                'password' => Hash::make(self::SEEDER_PASSWORD)
            ],
            [
                'name' => 'john',
                'email' => 'john@admin.com',
                'password' => Hash::make(self::SEEDER_PASSWORD)
            ]
        ];

        try {
            DB::table(with(new Admin)->getTable())->insert($seederData);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage());
        }

        return $returnBool ? true : $$this->successResponse('Admins created successfully !');
    }

    /**
     * Create 100 teachers
     *
     * @param bool $returnBool
     *
     * @return \Illuminate\Http\JsonResponse|bool
     */
    public function seedTeacher($returnBool = false)
    {
        $dataSeeder = [];

        for ($i = 0; $i < 100; $i++) {
            $dataSeeder[] = [
                'name' => $this->_faker->name(),
                'email' => 'teacher' . $i . '@mail.com',
                'password' => Hash::make(self::SEEDER_PASSWORD),
                'quart' => 'quart.jpg'
            ];
        }

        try {
            DB::table(with(new Teacher)->getTable())->insert($dataSeeder);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage());
        }

        return $returnBool ? true : $this->successResponse('Teachers created successfully');
    }

    /**
     * Create 100 students
     *
     * @param bool $returnBool
     *
     * @return \Illuminate\Http\JsonResponse|bool
     */
    public function seedStudent($returnBool = false)
    {
        $dataSeeder = [];

        for ($i = 0; $i < 100; $i++) {
            $dataSeeder[] = [
                'name' => $this->_faker->name(),
                'email' => 'student' . $i . '@mail.com',
                'password' => Hash::make(self::SEEDER_PASSWORD),
            ];
        }

        try {
            DB::table(with(new Student)->getTable())->insert($dataSeeder);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage());
        }

        return $returnBool ? true : $this->successResponse('Students created successfully');
    }

    /**
     * Create all categories
     *
     * @param bool $returnBool
     *
     * @return \Illuminate\Http\JsonResponse|bool
     */
    public function seedCategory($returnBool = false)
    {
        $categories = [
            [
                'title' => 'PHP',
                'slug' => Str::slug('PHP'),
                'image' => 'categ-php.svg'
            ],
            [
                'title' => 'JavaScript',
                'slug' => Str::slug('JavaScript'),
                'image' => 'categ-javascript.svg'
            ],
            [
                'title' => 'HTML',
                'slug' => Str::slug('HTML'),
                'image' => 'categ-html.svg'
            ],
            [
                'title' => 'CSS',
                'slug' => Str::slug('CSS'),
                'image' => 'categ-css.svg'
            ],
            [
                'title' => 'NodeJS',
                'slug' => Str::slug('NodeJS'),
                'image' => 'categ-nodejs.svg'
            ],
            [
                'title' => 'Java',
                'slug' => Str::slug('Java'),
                'image' => 'categ-java.svg'
            ],
        ];

        try {
            DB::table(with(new Category)->getTable())->insert($categories);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage());
        }

        return $returnBool ? true : $this->successResponse('Categories created successfully');
    }

    /**
     * Create all courses
     *
     * @param bool $returnBool
     *
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function seedCourse($returnBool = false)
    {
        $dataSeeder = [];

        $teacher = Teacher::query()->find(2);
        $teacher->validated = true;
        $teacher->save();

        $categories = Category::all();

        for ($i = 0; $i < 50; $i++) {
            $category = $categories->random();

            $dataSeeder[] = [
                'title' => $category->title . ' - ' . $this->_faker->realText(10),
                'content' =>
                    '<p>' . $this->_faker->realText(1000) . '</p>' .
                    '<p>' . $this->_faker->realText(1000) . '</p>' .
                    '<p>' . $this->_faker->realText(1000) . '</p>',
                'categorie_id' => $category->id,
                'teacher_id' => $teacher->id,
                'created_at' => now(),
                'public' => rand(0, 1)
            ];
        }

        try {
            DB::table(with(new Course)->getTable())->insert($dataSeeder);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage());
        }

        return $returnBool ? true : $this->successResponse('Courses created successfully');
    }
}
