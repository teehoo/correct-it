<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\CourseRepository;
use App\Repositories\StudentRepository;
use App\Repositories\TeacherRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class AdminDashboardController extends Controller
{
    /**
     * Return general view of admin dashboard
     *
     * @return Factory|View
     */
    public function index()
    {
        return view('admin.dashboard.dashboard');
    }

    /**
     * Return teacher manager view of admin dashboard
     *
     * @param TeacherRepository $teacherRepository
     *
     * @return Factory|View
     */
    public function indexTeacher(TeacherRepository $teacherRepository)
    {
        $dataTeacher = $teacherRepository->paginate(10);

        return view('admin.dashboard.dashboard-teacher', ['dataTeacher' => $dataTeacher]);
    }

    /**
     * Return student manager view of admin dashboard
     *
     * @param StudentRepository $studentRepository
     *
     * @return Factory|View
     */
    public function indexStudent(StudentRepository $studentRepository)
    {
        $dataStudent = $studentRepository->paginate(10);

        return view('admin.dashboard.dashboard-student', ['dataStudent' => $dataStudent]);
    }

    /**
     * Return course manager view of admin dashboard
     *
     * @param CourseRepository $courseRepository
     *
     * @return Factory|View
     */
    public function indexCourse(CourseRepository $courseRepository)
    {
        $courses = $courseRepository->with(['category', 'teacher'])->paginate(10);

        return view('admin.dashboard.dashboard-course', ['courses' => $courses]);
    }

    /**
     * Route to activate or deactivate account of teacher
     *
     * @param Request           $request
     * @param                   $idTeacher
     * @param                   $action
     * @param TeacherRepository $teacherRepository
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function teacherValidation(
        Request $request,
        TeacherRepository $teacherRepository,
        $idTeacher
    ) {
        $teacher = $teacherRepository->find($idTeacher);

        if (isset($teacher)) {
            $teacher->toggleActivation()->save();
        }

        return redirect()->route('admin.dashboard.teacher');
    }
}
