<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Return success JSON response
     *
     * @param mixed $data
     * @param int   $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function successResponse($data, $code = 200)
    {
        return response()->json([
            'result' => 'success',
            'data' => $data,
        ], $code);
    }

    /**
     * Return error JSON response
     *
     * @param mixed $data
     * @param int   $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function errorResponse($data, $code = 500)
    {
        return response()->json([
            'result' => 'error',
            'error' => $data,
        ], $code);
    }
}
