<?php

namespace App\Http\Controllers\Course;

use App\Course;
use App\Http\Controllers\Controller;
use App\Providers\AuthServiceProvider;
use App\Repositories\CategoryRepository;
use App\Repositories\CourseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CourseController extends Controller
{
    private $_courseRepository;
    private $_categoryRepository;

    public function __construct(
        CourseRepository $courseRepository,
        CategoryRepository $categoryRepository
    ){
        $this->_courseRepository = $courseRepository;
        $this->_categoryRepository = $categoryRepository;
    }

    public function all()
    {
        $courses = $this->_courseRepository->with('teacher')->onlyPublic()->paginate(5);
        $categories = $this->_categoryRepository->all();

        return view('course.course-all', [
            'courses' => $courses,
            'categories' => $categories
        ]);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show($id)
    {
        $course = $this->_courseRepository->with('category')->find($id);

        /* Retrieve random others courses to inject in <aside> in front */
        $randomCourses = DB::table('courses')
            ->inRandomOrder()
            ->select([
                'teachers.name as teacher_name',
                'courses.title',
                'courses.id',
                'courses.created_at',
                'courses.updated_at'
            ])
            ->where('courses.categorie_id', $course->categorie_id)
            ->where('courses.id', '!=', $course->id)
            ->where('public', true)
            ->join('teachers', 'courses.teacher_id', 'teachers.id')
            ->limit(5)
            ->get();

        foreach ($randomCourses as $randomCourse) {
            if (isset($randomCourse->created_at)) {
                $randomCourse->created_at = (new Carbon($randomCourse->created_at))->locale('fr')->diffForHumans();
            }
            if (isset($randomCourse->updated_at)) {
                $randomCourse->updated_at = (new Carbon($randomCourse->updated_at))->locale('fr')->diffForHumans();
            }
        }

        return view('course.course-show', [
            'course' => $course,
            'randomCourses' => $randomCourses
        ]);
    }

    public function edit($id)
    {
        $course = $this->_courseRepository->with('category')->find($id);
        $categories = $this->_categoryRepository->all();

        return view('course.course-edit', [
            'course' => $course,
            'categories' => $categories
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title'          => 'required|max:255',
            'hidden_content' => 'required',
            'categorie_id'   => 'required',
            'visibility'     => 'required'
        ]);

        $this->_courseRepository->update([
            'title'        => $request->input('title'),
            'content'      => $request->input('hidden_content'),
            'public'       => $request->input('visibility'),
            'categorie_id' => $request->input('categorie_id')
        ], $id);

        return redirect()->route('teacher.dashboard.course.list');
    }

    /**
     * Delete a course
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $this->_courseRepository->delete($id);

        $redirectRoute = getCurrentGuard() === AuthServiceProvider::GUARD_ADMIN_NAME
            ? 'admin.dashboard.course' : 'teacher.dashboard.course.list';

        return redirect()->route($redirectRoute);
    }

    /**
     * Return courses according to criteria in the search request
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $request->flash();
        $query = Course::query();

        if ($request->filled('keyword')) {
            $query->where('title', 'like', '%' . $request->input('keyword') . '%');
        }

        if ($request->filled('category') && $request->input('category') !== 'all') {
            $query->whereHas('category', function (Builder $query) use ($request) {
                $query->where('id', $request->input('category'));
            });
        }

        $courses = $query->where('public', true)->paginate(5);
        $categories = $this->_categoryRepository->all();

        return view('course.course-all', [
            'courses'    => $courses,
            'categories' => $categories,
            'search'     => $request->input('keyword')
        ]);
    }
}
