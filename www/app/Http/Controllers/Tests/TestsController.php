<?php

namespace App\Http\Controllers\Tests;

use App\Http\Controllers\Controller;
use App\Repository\Admin\AdminRepository;
use Illuminate\Http\Request;

class TestsController extends Controller
{

    private $adminRepository;

    /**
     * TestsController constructor.
     * @param AdminRepository $adminRepository
     */
    public function __construct(AdminRepository $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    public function testAdminRepository()
    {
        dd($this->adminRepository->with(['d'])->all());
        return response()->json([
            'data' => $this->adminRepository->all(),
            'time' => now(),
        ], 200);
    }
}
