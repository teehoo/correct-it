<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;
use App\Repositories\CourseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeacherDashboardController extends Controller
{
    private $_categoryRepository;
    private $_courseRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        CourseRepository $courseRepository
    ){
        $this->_categoryRepository = $categoryRepository;
        $this->_courseRepository = $courseRepository;
    }

    /**
     * Show teacher dashboard general view
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('teacher.dashboard.dashboard');
    }

    /**
     * Show teacher dashboard create course view
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexCourseCreate(Request $request)
    {
        $categories = $this->_categoryRepository->all();

        return view('teacher.dashboard.dashboard-course-create', ['categories' => $categories]);
    }

    /**
     * @ajaxRoute
     * Handle the creation of a course
     *
     * @param Request $request
     *
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function createCourse(Request $request)
    {
        $request->validate([
            'title'       => 'required|max:255',
            'content'     => 'required',
            'category_id' => 'required',
            'visibility'  => 'required'
        ]);

        $teacher = Auth::user();

        $course = $this->_courseRepository->create([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'teacher_id' => $teacher->id,
            'categorie_id' => $request->input('category_id'),
            'public' => $request->input('visibility')
        ]);

        return $course;
    }

    public function indexCourseList()
    {
        $teacher = Auth::user();
        $courses = $this->_courseRepository->with('category')->where('teacher_id', $teacher->id)->paginate(10);

        return view('teacher.dashboard.dashboard-course-list', ['courses' => $courses]);
    }
}
