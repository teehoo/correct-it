<?php

use App\Providers\AuthServiceProvider;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Auth Helpers
|--------------------------------------------------------------------------
*/

/**
 * Check if the current user is auth
 *
 * @param string  $scope
 * @return bool
 */
function isAuth($scope = null): bool
{
    if (is_null($scope) || $scope === '*') {
        return !is_null(Auth::guard(AuthServiceProvider::GUARD_ADMIN_NAME)->user())
            || !is_null(Auth::guard(AuthServiceProvider::GUARD_TEACHER_NAME)->user())
            || !is_null(Auth::guard(AuthServiceProvider::GUARD_STUDENT_NAME)->user());
    }

    if (in_array($scope, [
        AuthServiceProvider::GUARD_ADMIN_NAME,
        AuthServiceProvider::GUARD_TEACHER_NAME,
        AuthServiceProvider::GUARD_STUDENT_NAME] )) {
        return !is_null(Auth::guard($scope)->user());
    }

    return false;
}

/**
 * Check if the current user is guest
 *
 * @param string $scope
 * @return bool
 */
function isGuest($scope = null): bool
{
    return !isAuth($scope);
}


/**
 * Return the guard of current auth user or null
 *
 * @return string|null
 */
function getCurrentGuard(): ?string
{
    $guardList = array_keys(config('auth.guards'));

    return collect($guardList)->first(function($guard) {
        return \auth()->guard($guard)->check();
    });
}
