<?php

use Illuminate\Support\Facades\Route;

/**
 * Check if the current route name equal the one passed in parameter
 *
 * @param string $nameRoute
 *
 * @return bool
 */
function isRoute($nameRoute): bool
{
    return  Route::currentRouteName() === $nameRoute;
}
