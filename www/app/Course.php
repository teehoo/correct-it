<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = [];

    protected $casts = [
        'visibility' => 'boolean'
    ];

    /* SCOPES */
    public function scopePublic($query)
    {
        return $query->where('public', true);
    }

    /* RELATIONS */

    public function category()
    {
        return $this->belongsTo(Category::class, 'categorie_id');
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

}
