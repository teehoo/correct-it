@extends('layout.main')
@section('title', 'Login étudiant')
@section('namespace', 'student.login')

@section('page-content')
    <div class="img-container">
        <div data-depth="0.2">
            <img src="{{ asset('images/illustrations/sprinting.svg') }}" alt="illustration d'une femme qui sprint" class="img-sprinting">
        </div>
    </div>
    <div class="wrapper">
        <form action="{{ route('student.login') }}" method="POST">
            @csrf
            <div class="header-form">
                <h2 class="color-dark size-big weight-bold">Se connecter à l'espace étudiant</h2>
            </div>
            <div class="content-form">
                <x-form.form-group type="text" name="email" :required="true">Email</x-form.form-group>
                <x-form.form-group type="password" name="password" :required="true">Mot de passe</x-form.form-group>
                <button class="button button--red" type="submit">Se connecter</button>
            </div>
            <div class="footer-form">
                <a class="color-dark size-normal" href="{{ route('student.register') }}"><span class="color-primary">Pas encore de compte ?</span> J'en créé un maintenant.</a>
            </div>
        </form>
        <h2 class="color-dark size-title">De <span class="weight-bold">nouveaux</span> cours vous attendent !</h2>
    </div>
@endsection
