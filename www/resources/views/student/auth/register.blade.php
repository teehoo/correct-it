@extends('layout.main')
@section('title', 'Register étudiant')
@section('namespace', 'student.register')

@section('page-content')
    <div class="img-container">
        <div data-depth="0.2">
            <img src="{{ asset('images/illustrations/loving-blue.svg') }}" alt="illustration d'une femme qui sprint" class="img-sprinting">
        </div>
    </div>
    <div class="wrapper">
        <form action="{{ route('student.register') }}" method="POST">
            @csrf
            <div class="header-form">
                <h2 class="color-dark size-big weight-bold">S'inscrire en tant qu'étudiant</h2>
            </div>
            <div class="content-form">
                <x-form.form-group type="text" name="name" :required="true">Nom</x-form.form-group>
                <x-form.form-group type="text" name="email" :required="true">Email</x-form.form-group>
                <x-form.form-group type="password" name="password" :required="true">Mot de passe</x-form.form-group>
                <x-form.form-group type="password" name="password_confirmation" :required="true">Confirmation du mot de passe</x-form.form-group>
                <button class="button button--red" type="submit">S'inscrire</button>
            </div>
            <div class="footer-form">
                <a class="color-dark size-normal" href="{{ route('student.login') }}"><span class="color-primary">Déjà un compte ?</span> Je me connecte maintenant.</a>
            </div>
        </form>
        <h2 class="color-dark size-title">Vous allez <span class="color-primary weight-bold">aimez</span> apprendre avec <span class="color-primary weight-bold">Correct'it</span> !</h2>
    </div>
@endsection
