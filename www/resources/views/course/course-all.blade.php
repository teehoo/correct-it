@extends('layout.main')
@section('title', 'Cours')
@section('namespace', 'course.all')

@section('page-content')
    <div class="wrapper">
        <h1 class="color-dark weight-bold size-title">Tout les cours disponibles</h1>
        <form method="POST" action="{{ route('course.search') }}">
            @csrf
            <div class="search-bar">
                <input type="text" name="keyword" id="keyword" value="{{ old('keyword') }}">
                <select name="category">
                    <option value="all">Tout</option>
                    @if(isset($categories))
                        @foreach($categories as $category)
                            <option @if($category->id == old('category')) selected @endif
                            value="{{ $category->id }}">{{ $category->title }}</option>
                        @endforeach
                    @endif
                </select>
                <button type="submit">
                    <i class="fas fa-search"></i>
                </button>
            </div>
        </form>

        <div class="d-container">

            <div class="content-courses">
                @if($courses->count() > 0)

                    @foreach($courses as $course)
                        <a href="{{ route('course.show', ['id' => $course->id]) }}"><div class="container-course">
                                <div class="item-course-category">Développement - {{$course->category->title}}</div>
                                <span class="item-course-title">{{$course->title}}</span>
                                <div class="item-course-metadata">
                                    <span class="author">{{ $course->teacher->name }}</span>
                                    &nbsp;<span class="circle-separator"></span>&nbsp
                                    <span class="date">{{ isset($course->updated_at) ? $course->updated_at : $course->created_at }}</span>
                                </div>
                            </div></a>
                    @endforeach

                    <div class="pagination-container">
                        {{ $courses->links() }}
                    </div>
                @else

                    <p>Aucun cours disponible</p>

                @endif

            </div>

        </div>
    </div>
@endsection
