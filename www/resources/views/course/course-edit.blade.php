@extends('layout.main')
@section('title', 'Cours')
@section('namespace', 'course.edit')

@section('page-content')
    <div class="wrapper">
        <h1 class="page-title">Editer un cours</h1>
        <p class="page-description">Vous pouvez modifier le contenu de votre cours facilement en remplissant les champs ci-dessous.</p>

        <form action="{{ route('course.update', ['id' => $course->id]) }}" method="POST">
            @csrf
            <div class="content-form">
                {{-- Input for title --}}
                <div class="fieldset fieldset-text">
                    <input class="form-input @if($errors->has('title')) has-error @endif"
                           type="text"
                           name="title"
                           id="title"
                           value="{{ old('title', $course->title) }}"
                           required>
                    <label class="form-label" for="title">Titre</label>
                </div>

                {{-- Select input for visibility --}}
                <div class="fieldset-visibility">
                    <i class="fas fa-eye"></i>
                    <label class="color-dark" for="visibility">Visibilité</label>
                    <select class="custom-select" name="visibility" id="visibility">
                        <option @if($course->public == 1) selected @endif value="1">Publique</option>
                        <option @if($course->public == 0) selected @endif value="0">Privé</option>
                    </select>
                </div>

                {{-- Select input for category --}}
                <div class="fieldset-category">
                    <label class="color-dark" for="categorie_id">Catégorie</label>
                    <select class="custom-select" name="categorie_id" id="categorie_id">
                        @if(isset($categories))
                            @foreach($categories as $category)
                                <option @if($category->id == $course->categorie_id) selected @endif value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                {{-- Text editor --}}
                <label for="content">Contenu</label>
                <div id="content"></div>
                <textarea name="hidden_content" id="hidden-content">
                    {{ $course->content }}
                </textarea>
            </div>

            <div class="footer-form">
                <button id="create-course" class="button button--red" type="submit">Mettre à jour le cours</button>
                <div class="alert--error">Veuillez remplir tous les champs s'il vous plait.</div>
                <div class="alert--success">Cours créé avec succès.</div>
            </div>
        </form>
    </div>
@endsection
