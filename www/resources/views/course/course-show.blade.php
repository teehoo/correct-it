@extends('layout.main')
@section('title', 'Cours')
@section('namespace', 'course.show')

@section('page-content')
    <div class="wrapper">
        <div class="course-container">
            <h1 class="course-title">{{ $course->title }}</h1>
            <div class="content">
                {!! $course->content !!}
            </div>
        </div>
        <aside>
            <h3 class="aside-title">Autres cours de {{ $course->category->title }}</h3>
            <ul class="random-course-list">
                @if(isset($randomCourses))
                    @foreach($randomCourses as $randomCourse)
                        <li>
                            <a href="{{ url('/course/' . $randomCourse->id) }}">
                                <span class="item-course-title">{{$randomCourse->title}}</span>
                                <div class="item-course-metadata">
                                    <span class="author">{{ $randomCourse->teacher_name }}</span>
                                     &nbsp;<span class="circle-separator"></span>&nbsp
                                    <span class="date">{{ isset($randomCourse->updated_at) ? $randomCourse->updated_at : $randomCourse->created_at }}</span>
                                </div>
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>

        </aside>

    </div>
@endsection
