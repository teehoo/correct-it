<div class="fieldset fieldset-{{ $type }}">
    {{-- For text and password --}}
    @if($type === 'text' || $type === 'password' || $type === 'file')
    <input class="form-input @if($errors->has($name)) has-error @endif"
           type="{{ $type }}"
           name="{{ $name }}"
           id="{{ $id }}"
           value="{{ old($name, isset($attributes['value']) ? $attributes['value'] : '') }}"
           {{ $attributes }}
           @if($required)required @endif>
    <label class="form-label" for="$id">{{ $slot }}</label>
    @endif

    {{-- For textarea --}}
    @if($type === 'textarea')
        <label class="form-label" for="$id">{{ $slot }}</label>
        <textarea class="form-input @if($errors->has($name)) has-error @endif"
               type="{{ $type }}"
               name="{{ $name }}"
               id="{{ $id }}"
               {{ $attributes }}
               @if($required)required @endif>{{ old($name) }}</textarea>
    @endif

    @if($errors->has($name))
        <small>{{ $errors->first($name) }}</small>
    @endif
</div>
