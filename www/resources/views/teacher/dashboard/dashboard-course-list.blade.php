{{-- Layout choosen --}}
@extends('layout.dashboard')
{{-- Title of the current page --}}
@section('title', 'Teacher - Dashboard')
{{-- Namespace of the current page --}}
@section('namespace', 'teacher.dashboard.course.list')

{{-- Menu of the Dashboard --}}
@section('dashboard-menu')
    @include('partials.menus.menu-teacher-dashboard')
@endsection

{{-- Content of the dashboard --}}
@section('dashboard-content')
    <h1 class="dashboard-content-title">Liste de vos cours</h1>
    <p class="dashboard-content-description">Vous pouvez avoir une vue d'ensemble sur vos cours et les modifer depuis cette page.</p>
    <div class="d-container">
        @if($courses->count() > 0)
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Titre</th>
                    <th scope="col">Catégorie</th>
                    <th scope="col">Visibilité</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($courses as $course)
                    <tr class="p">
                        <td>{{ $course->title }}</td>
                        <td>{{ $course->category->title }}</td>
                        <td>{{ $course->public ? 'Publique' : 'Privé' }}</td>
                        <td>
                            <a class="link-button" href="{{ route('course.show', ['id' => $course->id]) }}">
                                <button type="button" class="btn btn-info">Voir</button>
                            </a>
                            <a class="link-button" href="{{ route('course.edit', ['id' => $course->id]) }}">
                                <button type="button" class="btn btn-warning">Editer</button>
                            </a>
                            <a class="link-button" href="{{ route('course.delete', ['id' => $course->id]) }}">
                                <button data-id="{{ $course->id }}" type="button" class="btn btn-danger delete-course">Supprimer</button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <span>Vous n'avez créé aucun cours pour le moment. <a class="color-primary" href="{{ route('teacher.dashboard.course.create') }}">Créer en un maintenant.</a></span>
        @endif
        {{ $courses->links() }}
    </div>

@endsection
