{{-- Layout choosen --}}
@extends('layout.dashboard')
{{-- Title of the current page --}}
@section('title', 'Teacher - Dashboard')
{{-- Namespace of the current page --}}
@section('namespace', 'teacher.dashboard.course.create')

{{-- Menu of the Dashboard --}}
@section('dashboard-menu')
    @include('partials.menus.menu-teacher-dashboard')
@endsection

{{-- Content of the dashboard --}}
@section('dashboard-content')
    <h1 class="dashboard-content-title">Créer un cours</h1>
    <p class="dashboard-content-description">Vous pouvez créer un cours en toute simplicité en remplissant les champs ci-dessous.</p>
    <div class="d-container">
        <form action="">
            @csrf
            <div class="content-form">
                {{-- Input for title --}}
                <x-form.form-group type="text" name="title" :required="true">Titre</x-form.form-group>
                {{-- Select input for visibility --}}
                <div class="fieldset-visibility">
                    <i class="fas fa-eye"></i>
                    <label class="color-dark" for="category_id">Visibilité</label>
                    <select class="custom-select" name="visibility" id="visibility">
                        <option value="1">Publique</option>
                        <option value="0">Privée</option>
                    </select>
                </div>
                {{-- Select input for category --}}
                <div class="fieldset-category">
                    <label class="color-dark" for="category_id">Catégorie</label>
                    <select class="custom-select" name="category_id" id="category_id">
                        @if(isset($categories))
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                {{-- Text editor --}}
                <label for="content">Contenu</label>
                <div id="content"></div>
            </div>

            <div class="footer-form">
                <button id="create-course" class="button button--red" type="submit">Créer le cours</button>
                <div class="alert--error">Veuillez remplir tous les champs s'il vous plait.</div>
                <div class="alert--success">Cours créé avec succès.</div>
            </div>
        </form>
    </div>

@endsection
