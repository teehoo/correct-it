@extends('layout.main')
@section('title', 'Espace formateur')
@section('namespace', 'teacher.space')

@section('page-content')
    <div class="wrapper">
        <h2 class="color-dark weight-medium size-title">Proposer des cours en lignes, c'est <span class="color-primary">facile</span> !</h2>
        <p class="color-dark size-big">Partagez votre savoir à des milliers d'internautes en quelques clics</p>
        <a class="button button--red" href="{{ route('teacher.register') }}">Devenir formateur</a>
    </div>
@endsection
