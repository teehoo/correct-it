@extends('layout.main')
@section('title', 'Formateur - Connexion')
@section('namespace', 'teacher.login')

@section('page-content')
    <div class="img-container">
        <div data-depth="0.2">
            <img src="{{ asset('images/illustrations/reading.svg') }}" alt="illustration d'une femme qui lit" class="img-reading">
        </div>
    </div>
    <div class="wrapper">
        <form action="{{ route('teacher.login') }}" method="POST">
            @csrf
            <div class="header-form">
                <h2 class="color-dark size-big weight-bold">Se connecter à l'espace formateur</h2>
            </div>
            <div class="content-form">
                <x-form.form-group type="text" name="email" :required="true">Email</x-form.form-group>
                <x-form.form-group type="password" name="password" :required="true">Mot de passe</x-form.form-group>
                <button class="button button--red" type="submit">Se connecter</button>
            </div>
            <div class="footer-form">
                <a class="color-dark size-normal" href="{{ route('teacher.register') }}"><span class="color-primary">Pas encore de compte ?</span> J'en créé un maintenant.</a>
            </div>
        </form>
        <h2 class="color-dark size-title">Rejoignez les plus de 500 formateurs déjà inscrits !</h2>
    </div>
@endsection
