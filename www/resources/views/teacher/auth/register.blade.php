@extends('layout.main')
@section('title', 'Formateur - Inscription')
@section('namespace', 'teacher.register')

@section('page-content')
    <div class="img-container">
        <div data-depth="0.2">
            <img src="{{ asset('images/illustrations/reading.svg') }}" alt="illustration d'une femme qui lit" class="img-reading">
        </div>
    </div>
    <div class="wrapper">
        <form action="{{ route('teacher.register') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="header-form">
                <h2 class="color-dark size-big weight-bold">Créer un compte formateur</h2>
            </div>
            <div class="content-form">
                <x-form.form-group type="text" name="name" :required="true">Nom</x-form.form-group>
                <x-form.form-group type="text" name="email" :required="true">Email</x-form.form-group>
                <x-form.form-group type="password" name="password" :required="true">Mot de passe</x-form.form-group>
                <x-form.form-group type="password" name="password_confirmation" :required="true">Confirmation du mot de passe</x-form.form-group>
                <p>Pour vérifier votre identité, veuillez insérer une photo recto verso de votre carte d'identité</p>
                <x-form.form-group
                    type="file"
                    name="quart"
                    label="Glisser-Déposer votre photo de carte d'identité"
                    is="drop-files"
                    :required="false"></x-form.form-group>
                <button class="button button--red" type="submit">S'enregistrer</button>
            </div>
            <div class="footer-form">
                <a class="color-dark size-normal" href="{{ route('teacher.login') }}"><span class="color-primary">Déjà un compte ?</span> Je me connecte maintenant.</a>
            </div>
        </form>
        <h2 class="color-dark size-title">Rejoignez les plus de 500 formateurs déjà inscrits !</h2>
    </div>
@endsection
