<header>
    @if(isAuth('teacher') && !Auth::guard('teacher')->user()->validated)
        <div class="alert--warning">Vous pourrez accéder au dashboard formateur une fois que votre inscription aura été validée par l'un de nos administrateurs.</div>
    @endif
    <div class="header-container wrapper">
        <a href="{{ route('home') }}"><img src="{{ asset('images/logos/logo.svg') }}" alt="" class="logo"></a>
        <div class="link-container">
            @if(isGuest())
                <a href="{{ route('teacher.space') }}" class="teacher-space color-primary">Espace Formateur</a>
                <a href="{{ route('student.register') }}" class="register color-dark weight-bold">Inscription</a>
                <a href="{{ route('student.login') }}" class="login color-dark">Connexion</a>
            @endif

            @if(isAuth())
                @if(isAuth('admin'))
                    <a class="color-primary weight-bold" href="{{ route('admin.dashboard') }}">Tableau de bord</a>
                @endif
                @if(isAuth('teacher'))
                    <a class="color-primary weight-bold" href="{{ route('teacher.dashboard') }}">Tableau de bord</a>
                @endif
                <div>Bonjour <span>{{Auth::guard(getCurrentGuard())->user()->name}}</span></div>
                <a class="logout" href="{{ route('logout') }}">Se déconnecter</a>
            @endif
        </div>
    </div>
</header>
