<footer>
    <div class="footer-container wrapper">
        <div class="link-container">
            <a href="" class="contact">Contact</a>
            <a href="" class="social-net">Réseaux sociaux</a>
            <div class="img-social-net">
                <a href=""><img src="{{ asset('/images/pictos/facebook.svg') }}" alt="logo facebook"></a>
                <a href=""><img src="{{ asset('/images/pictos/twitter.svg') }}" alt="logo twitter"></a>
                <a href=""><img src="{{ asset('/images/pictos/instagram.svg') }}" alt="logo instagram"></a>
            </div>
        </div>
        <img src="{{ asset('images/logos/logo.svg') }}" alt="logo correct it" class="logo">
    </div>
</footer>
