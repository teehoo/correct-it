<nav class="menu">
    <div class="menu-header">
        <span>Dashboard Admin</span>
    </div>
    <ul class="menu-list">
{{--        <li class="menu-list-item">
            <a href="{{ route('admin.dashboard') }}" class="@if(isRoute('admin.dashboard')) active @endif">
                <i class="icon-item fas fa-cogs"></i>
                <span class="text-item">Général</span>
            </a>
        </li>--}}
        <li class="menu-list-item">
            <a href="{{ route('admin.dashboard.teacher') }}" class="@if(isRoute('admin.dashboard.teacher')) active @endif">
                <i class="icon-item fas fa-chalkboard-teacher"></i>
                <span class="text-item">Formateurs</span>
            </a>
        </li>
        <li class="menu-list-item">
            <a href="{{ route('admin.dashboard.student') }}" class="@if(isRoute('admin.dashboard.student')) active @endif">
                <i class="icon-item fas fa-user-graduate"></i>
                <span class="text-item">Etudiants</span>
            </a>
        </li>
        <li class="menu-list-item">
            <a href="{{ route('admin.dashboard.course') }}" class="@if(isRoute('admin.dashboard.course')) active @endif">
                <i class="icon-item fas fa-chalkboard-teacher"></i>
                <span class="text-item">Cours</span>
            </a>
        </li>
    </ul>
    <div class="menu-footer">

    </div>
</nav>
