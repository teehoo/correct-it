<nav class="menu">
    <div class="menu-header">
        <span>Dashboard Formateur</span>
    </div>
    <ul class="menu-list">
{{--        <li class="menu-list-item">
            <a href="{{ route('teacher.dashboard') }}" class="@if(isRoute('teacher.dashboard')) active @endif">
                <i class="icon-item fas fa-cogs"></i>
                <span class="text-item">Général</span>
            </a>
        </li>--}}
        <li class="menu-list-item">
            <a href="{{ route('teacher.dashboard.course.create') }}" class="@if(isRoute('teacher.dashboard.course.create')) active @endif">
                <i class="icon-item fas fa-chalkboard-teacher"></i>
                <span class="text-item">Créer un cours</span>
            </a>
        </li>
        <li class="menu-list-item">
            <a href="{{ route('teacher.dashboard.course.list') }}" class="@if(isRoute('teacher.dashboard.course.list')) active @endif">
                <i class="fas fa-list"></i>
                <span class="text-item">Tous mes cours</span>
            </a>
        </li>
    </ul>
    <div class="menu-footer">

    </div>
</nav>
