{{-- Layout choosen --}}
@extends('layout.dashboard')
{{-- Title of the current page --}}
@section('title', 'Admin - Dashboard')
{{-- Namespace of the current page --}}
@section('namespace', 'admin.dashboard.student')

{{-- Menu of the Dashboard --}}
@section('dashboard-menu')
    @include('partials.menus.menu-admin-dashboard')
@endsection

{{-- Content of the dashboard --}}
@section('dashboard-content')
    <h1 class="dashboard-content-title">Gestion des étudiants</h1>
    <p class="dashboard-content-description">Vous pouvez avoir une vue d'ensemble des étudiants inscrits sur le site</p>
    <div class="d-container">
        @if($dataStudent->count() > 0)
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Email</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataStudent as $student)
                    <tr class="p">
                        <td>{{ $student->name }}</td>
                        <td>{{ $student->email }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <span>Aucun résultat trouvé</span>
        @endif
        {{ $dataStudent->links() }}
    </div>
@endsection
