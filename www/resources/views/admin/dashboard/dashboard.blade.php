{{-- Layout choosen --}}
@extends('layout.dashboard')
{{-- Title of the current page --}}
@section('title', 'Admin - Dashboard')
{{-- Namespace of the current page --}}
@section('namespace', 'admin.dashboard')

{{-- Menu of the Dashboard --}}
@section('dashboard-menu')
@include('partials.menus.menu-admin-dashboard')
@endsection

{{-- Content of the dashboard --}}
@section('dashboard-content')
    <h1 class="dashboard-content-title">Général</h1>

@endsection
