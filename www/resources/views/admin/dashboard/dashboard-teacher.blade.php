{{-- Layout choosen --}}
@extends('layout.dashboard')
{{-- Title of the current page --}}
@section('title', 'Admin - Dashboard')
{{-- Namespace of the current page --}}
@section('namespace', 'admin.dashboard.teacher')

{{-- Menu of the Dashboard --}}
@section('dashboard-menu')
    @include('partials.menus.menu-admin-dashboard')
@endsection

{{-- Content of the dashboard --}}
@section('dashboard-content')
    <h1 class="dashboard-content-title">Gestion des formateurs</h1>
    <p class="dashboard-content-description">Vous pouvez avoir une vue d'ensemble des formateurs et valider les nouveaux</p>
    <div class="d-container">
        @if($dataTeacher->count() > 0)
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Email</th>
                <th scope="col">Carte d'identité</th>
                <th scope="col">Activation du compte</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dataTeacher as $teacher)
                <tr class="p">
                    <td>{{ $teacher->name }}</td>
                    <td>{{ $teacher->email }}</td>
                    <td>
                        <a class="display-quart" rel="modal:open" href="{{ asset('images/teachers/quarte/' . $teacher->quart) }}">
                            <button type="button" class="btn btn-info">Voir l'image</button>
                        </a>
                    </td>
                    <td>
                        @if($teacher->validated)
                        <a class="activate" href="{{ route('admin.dashboard.teacher.validation', ['id' => $teacher->id, 'action' => 'deactivate']) }}">
                            <button type="button" class="btn btn-danger">Désactiver le compte</button>
                        </a>
                        @else
                        <a class="activate" href="{{ route('admin.dashboard.teacher.validation', ['id' => $teacher->id, 'action' => 'activate']) }}">
                            <button type="button" class="btn btn-success">Activer le compte</button>
                        </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <span>Aucun résultat trouvé</span>
        @endif
        {{ $dataTeacher->links() }}
    </div>
    <div id="modal-quart"></div>
@endsection
