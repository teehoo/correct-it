@extends('layout.main')
@section('title', 'Admin - Connexion')
@section('namespace', 'admin.login')

@section('page-content')
    <div class="wrapper">
        <div>
            <form action="{{ route('admin.login') }}" method="POST">
                @csrf
                <div class="header-form">
                    <img src="{{ asset('images/logos/logo-reduct.svg') }}" alt="">
                    <h2 class="color-dark size-big weight-bold">Espace administrateur</h2>
                </div>
                <div class="content-form">
                    <x-form.form-group type="text" name="email" :required="true">Email</x-form.form-group>
                    <x-form.form-group type="password" name="password" :required="true">Mot de passe</x-form.form-group>
                    <button class="button button--red" type="submit">Se connecter</button>
                </div>
            </form>
        </div>
    </div>
@endsection
