@extends('layout.main')
@section('title', 'Home')
@section('namespace', 'home')

@section('page-content')
    <section>
        <img class="circle-grey-shape" src="{{ asset('images/shapes/circle-grey-shape.svg') }}" alt="forme ronde grise en fond">
        <div class="section-container wrapper">
            <img src="{{ 'images/illustrations/clumsy.svg' }}" alt="Illustration d'un homme qui fait tomber toutes ses feuilles de cours">
            <div>
                <h2 class="color-dark weight-bold size-title">Les cours sur feuille vous font défaut ?</h2>
                <p class="color-dark size-big"><span class="weight-bold">Correct`it</span> est une solution <span class="color-primary">e-learning</span> pour l'apprentissage des technologies web</p>
                <a class="button button--empty" href="{{ route('course.all') }}">Voir les cours</a>
            </div>
        </div>
    </section>
    <section>
        <img class="circle-red-shape" src="{{ asset('images/shapes/circle-red-shape.svg') }}" alt="forme ronde rouge en fond">
        <div class="section-container wrapper">
            <div>
                <h2 class="color-dark weight-bold size-title">Soyez libres d'apprendre tout ce que vous voulez</h2>
                <p class="color-dark size-big">Débutants, amateurs ou même professionnels il y a toujours un point à <span class="weight-bold">apprendre</span> ou à <span class="weight-bold">approfondir</span>. Ne soyez pas timide !</p>
            </div>
            <img src="{{ 'images/illustrations/bikini.svg' }}" alt="Illustration d'un homme qui fait tomber toutes ses feuilles de cours">
        </div>
    </section>
    <section>
        <div class="section-container wrapper">
            <div class="text-header">
                <h2 class="color-dark weight-bold size-title">Plus de 30 technologies enseignées</h2>
                <p class="color-dark size-big">Frontend ou Backend ? Il y en a pour <span class="weight-bold">tous les goûts</span></p>
            </div>
            <div class="carousel-container">
                <div class="carousel-arow-prev">
                    <img src="{{ asset('images/pictos/arrow-prev.svg') }}" alt="fleche précédente">
                </div>
                <div id="home-carousel">
                    <div class="carousel-cell"><img src="{{ asset('images/pictos/javascript.svg') }}" alt="pictogramme javascript"></div>
                    <div class="carousel-cell"><img src="{{ asset('images/pictos/css.svg') }}" alt="pictogramme css"></div>
                    <div class="carousel-cell"><img src="{{ asset('images/pictos/php.svg') }}" alt="pictogramme php"></div>
                    <div class="carousel-cell"><img src="{{ asset('images/pictos/nodejs.svg') }}" alt="pictogramme nodejs"></div>
                    <div class="carousel-cell"><img src="{{ asset('images/pictos/html.svg') }}" alt="pictogramme html"></div>
                </div>
                <div class="carousel-arow-next">
                    <img src="{{ asset('images/pictos/arrow-next.svg') }}" alt="flèche suivante">
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="section-container wrapper color-white">
            <img class="img-chilling" src="{{ asset('images/illustrations/chilling.svg') }}" alt="illustration d'une personne allongée">
            <h2 class="size-title color-white weight-bold">Une question ? Besoin d'aide ?</h2>
            <p class="size-big weight-regular">Notre support est disponible du <span class="weight-bold">lundi</span> au <span class="weight-bold">vendredi</span> de <span class="text-underline">8h à 19h</span></p>
            <a class="button button--empty color-white" href="mailto:support@correctit.com">Contactez-nous</a>
        </div>
    </section>
@endsection
