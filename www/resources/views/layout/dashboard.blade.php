@extends('layout.main')

@section('title')
    @yield('title-content')
@endsection

@section('namespace')
    @yield('namespace')
@endsection

@section('page-content')
    <div class="dashboard-wrapper">
        {{-- Dashboard Menu --}}
        <div class="dashboard-menu-container">
            @yield('dashboard-menu')
        </div>
        {{-- Dashboard Content --}}
        <div class="dashboard-content">
            @yield('dashboard-content')
        </div>
    </div>
@endsection
