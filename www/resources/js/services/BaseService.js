import domElements from "../modules/domElements";

export default class BaseService {
    constructor() {
        this.DOM = domElements;
    }
}
