import axios from 'axios';

export default class ApiService {

    static getApiEndpoint () {
        return window.location.origin;
    }

    /**
     * Make HTTP request
     * @param {Object}options ('url', 'method', 'headers', data
     * @returns {AxiosPromise}
     */
    static async request (options) {
        options.url = `${this.getApiEndpoint()}${options.url}`;

        return axios(options);
    }
}
