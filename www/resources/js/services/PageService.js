import BaseService from "./BaseService";

export default class PageService extends BaseService {
    constructor() {
        super();
    }

    /**
     * Get the namespace of the current page
     *
     * @return {string}
     */
    getNamespace() {
        return this.DOM.pageContainer.data('namespace');
    }

    /**
     * Get the class of page-container according to the namespace
     *
     * @param string namespace
     * @return {string}
     */
    static getPageClassByNamespace(namespace) {
        return `div.site-container[data-namespace="${namespace}"]`;
    }
}
