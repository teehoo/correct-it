import BaseComponent from "../BaseComponent";
import PageService from "../../services/PageService";

export default class Footer extends BaseComponent {
    constructor() {
        super();
        this.initElements();
        this.initEvent();
    }

    initEvent() {
        this.hide();
    }

    initElements() {
        this.blackListedPage = this.getBlackListedPage();
    }

    /**
     * Return list of page on which footer need to be hidden
     *
     * @return {string[]}
     */
    getBlackListedPage() {
        return [
            // Admin
            'admin.login',
            'admin.dashboard',
            'admin.dashboard.teacher',
            'admin.dashboard.student',
            'admin.dashboard.course',

            // Student
            'student.login',
            'student.register',

            // Teacher
            'teacher.login',
            'teacher.register',
            'teacher.dashboard',
            'teacher.dashboard.course.create',
            'teacher.dashboard.course.list'
        ];
    }

    /**
     * Hide footer
     */
    hide() {
        const currentPage = new PageService().getNamespace();
        this.blackListedPage.some(page => {
            if (page === currentPage) {
                this.DOM.footer.hide();
            }
        })
    }
}
