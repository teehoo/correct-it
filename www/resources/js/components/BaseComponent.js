import domElements from "../modules/domElements";

export default class BaseComponent {
    constructor() {
        this.DOM = domElements;
    }
}
