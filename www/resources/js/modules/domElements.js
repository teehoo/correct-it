import $ from 'jquery';
import PageService from "../services/PageService";

let classPage = {
    // Home
    'home': PageService.getPageClassByNamespace('home'),

    // Student
    'student.login': PageService.getPageClassByNamespace('student.login'),
    'student.register': PageService.getPageClassByNamespace('student.register'),

    // Teacher
    'teacher.login': PageService.getPageClassByNamespace('teacher.login'),
    'teacher.register': PageService.getPageClassByNamespace('teacher.register'),
};

export default {
    // common
    body: $('body'),
    header: $('header'),
    footer: $('footer'),
    pageContainer: $('.site-container'),

    // Home
    homeCarousel: $(`${classPage.home} #home-carousel`),
    homeCarouselArrowPrev: $(`${classPage['home']} .carousel-arow-prev`),
    homeCarouselArrowNext: $(`${classPage['home']} .carousel-arow-next`),
    homeCircleGreyShape: $(`${classPage['home']} img.circle-grey-shape`),
    homeCircleRedShape: $(`${classPage['home']} img.circle-red-shape`),

    // Student
    // -- Login
    studLogImageContainer: $(`${classPage['student.login']} div.img-container`),
    // -- Register
    studRegisterImageContainer: $(`${classPage['student.register']} div.img-container`),

    // Teacher
    teachLogImageContainer: $(`${classPage['teacher.login']} div.img-container`),
    teachRegisterImageContainer: $(`${classPage['teacher.register']} div.img-container`),
};
