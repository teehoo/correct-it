import Router from "./Router";
import CommonPageModule from "./pages/CommonPageModule";
import HomePageModule from "./pages/HomePageModule";
import LoginStudentPageModule from "./pages/student/LoginStudentPageModule";
import RegisterStudentPageModule from "./pages/student/RegisterStudentPageModule";
import LoginTeacherPageModule from "./pages/teacher/LoginTeacherPageModule";
import RegisterTeacherPageModule from "./pages/teacher/RegisterTeacherPageModule";
import CreateCourseTeacherPageModule from "./pages/teacher/dashboard/CreateCourseTeacherPageModule";
import ListCourseTeacherPageModule from "./pages/teacher/dashboard/ListCourseTeacherPageModule";
import EditCoursePageModule from "./pages/course/EditCoursePageModule";
import ListTeacherPageModule from "./pages/admin/dashboard/ListTeacherPageModule";

const AppRouter = new Router();

AppRouter
    // Home
    .get('home', HomePageModule)

    // Admin
    .get('admin.dashboard.teacher', ListTeacherPageModule)

    // Student
    .get('student.login', LoginStudentPageModule)
    .get('student.register', RegisterStudentPageModule)

    // Teacher
    .get('teacher.login', LoginTeacherPageModule)
    .get('teacher.register', RegisterTeacherPageModule)
    .get('teacher.dashboard.course.create', CreateCourseTeacherPageModule)
    .get('teacher.dashboard.course.list', ListCourseTeacherPageModule)
    .get('course.edit', EditCoursePageModule)

    // Common
    .all(CommonPageModule);

export default AppRouter;
