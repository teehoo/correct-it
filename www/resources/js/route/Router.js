import domElements from "../modules/domElements";
import PageService from "../services/PageService";

export default class AppRouter {
    constructor() {
        this.routes = [];
        this.common = null;
        this.PageService = new PageService();
    }

    get(namespace, pageModule) {
        if(!namespace || !pageModule) throw new Error('namespace or callback must be given');

        this.routes.forEach(route => {
            if (route.namespace === namespace) throw new Error(`the url ${namespace} already exists`);
        });

        const route = {
            namespace,
            pageModule
        };

        this.routes.push(route);

        return this;
    }

    all(pageModule) {
        this.common = pageModule;

        return this;
    }

    init() {
        let request = new URL(window.location.pathname, window.location.origin);
        if(this.common) {
            new this.common(request);
        }
        this.routes.some(route => {
            let regExNamespace = new RegExp(`^${route.namespace}$`);
            let namespace = this.PageService.getNamespace();
            if (namespace.match(regExNamespace)) {
                new route.pageModule(request);
            }
        });
    }
}
