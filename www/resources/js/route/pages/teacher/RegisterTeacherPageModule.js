import BasePageModule from "../BasePageModule";
import Parallax from "parallax-js";
import '@grafikart/drop-files-element';

export default class RegisterTeacherPageModule extends BasePageModule {
    constructor(request) {
        super(request);
        this.initElements();
        this.initEvents();
        this.fire();
    }

    initElements() {

    }

    initEvents() {

    }

    fire() {
        this.handleParallax();
    }

    /**
     * Handle parallax
     * @return {void}
     */
    handleParallax() {
        const scene = this.DOM.teachRegisterImageContainer[0];
        let parallax = new Parallax(scene);
    }
}
