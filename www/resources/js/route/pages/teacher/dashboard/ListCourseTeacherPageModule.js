import BasePageModule from "../../BasePageModule";
import $ from 'jquery';
import ApiService from "../../../../services/ApiService";

export default class ListCourseTeacherPageModule extends BasePageModule {
    constructor(request) {
        super(request);
        this.initElements();
        this.initEvents();
        this.fire();
    }

    initElements() {
        this.deleteButtons = $('button.delete-course');
    }

    initEvents() {
        /*this._onCLickDeleteButton();*/
    }

    fire() {

    }

/*    _onCLickDeleteButton() {
        this.deleteButtons.on('click', event => {
            event.preventDefault();
            const id = $(event.target).data('id');


            ApiService.request({
                method: 'DELETE',
                url: `/course/${id}`
            })
        })
    }*/
}
