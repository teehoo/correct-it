import BasePageModule from "../../BasePageModule";
import $ from 'jquery';
import Quill from 'quill';
import ApiService from "../../../../services/ApiService";
import TextEditorModule from "../../../../modules/TextEditorModule";

export default class CreateCourseTeacherPageModule extends BasePageModule{
    constructor(request) {
        super(request);
        this.initElements();
        this.initEvents();
        this.fire();
    }

    initElements() {
        /* Quill editor */
        this.textareaContainer = $('div#content');
        this.editor = this._createTextEditor();
        /* Input forms */
        this.titleInput = $('input[name="title"]');
        this.categorySelect = $('select[name="category_id"]');
        this.visibilitySelect = $('select[name="visibility"]');
        this.createCourseButton = $('button#create-course');
        /* Alerts */
        this.alertError = $('div.alert--error');
        this.alertSuccess = $('div.alert--success');
    }

    initEvents() {
        this._onClickCreateCourseButton();
    }

    fire() {

    }

    _createTextEditor() {
        return TextEditorModule.factory(`#${this.textareaContainer.attr('id')}`, 'Contenu du cours ...');
    }

    _onClickCreateCourseButton() {
        this.createCourseButton.on('click', async event => {
            event.preventDefault();

            /* Retrieve all value of form */
            const formData = {
                title: this.titleInput.val(),
                content: this.editor.getHtmlContent(),
                category_id: this.categorySelect.val(),
                visibility: this.visibilitySelect.val(),
            };
            console.log(formData);

            try {
                const data = await ApiService.request({
                    method: 'POST',
                    url: '/teacher/dashboard/course/create',
                    data: formData
                });

                this.alertError.hide();
                this.alertSuccess.show();
                this._resetFormInputs();

                /* Hide success alert after 3 sec */
                setInterval(() => {
                    this.alertSuccess.hide()
                }, 3000);

            } catch (error) {
                /*  Hide success alert if an error occured */
                this.alertSuccess.hide();

                if (error.response && error.response.data) {
                    this.alertError.text(error.response.data.message);
                }

                this.alertError.show();
            }
        });
    }

    /**
     * Reset all inputs after success of course creation
     */
    _resetFormInputs() {
        this.titleInput.val('');
        this.categorySelect.val('1');
        /* Clear text editor */
        this.visibilitySelect.val('1');this.editor.setContents([]);
        this.visibilitySelect.val('1');this.editor.setText('');
    }
}
