import BasePageModule from "./BasePageModule";
import Header from "../../components/partials/Header";
import Footer from "../../components/partials/Footer";
import PageContainer from "../../components/partials/PageContainer";

export default class CommonPageModule extends BasePageModule{
    constructor(request) {
        super(request);
        this.initElements();
        this.initEvents();
        this.fire();
    }

    initElements() {

    }

    initEvents() {

    }

    fire() {
        new Header();
        new Footer();
        new PageContainer();
    }
}
