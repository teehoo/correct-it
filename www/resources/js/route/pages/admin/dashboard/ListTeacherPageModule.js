import $ from 'jquery';
import BasePageModule from "../../BasePageModule";

export default class ListTeacherPageModule extends BasePageModule {
    constructor(request) {
        super(request);
        this.initElements();
        this.initEvents();
        this.fire();
    }

    initElements() {
        this.displayQuartButton = $('a.display-quart');
        this.modal = $('div#modal-quart');
    }

    initEvents() {
    }

    fire() {

    }

    _initModal() {

    }
}
