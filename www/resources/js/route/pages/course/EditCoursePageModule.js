import BasePageModule from "../BasePageModule";
import TextEditorModule from "../../../modules/TextEditorModule";
import $ from "jquery";

export default class EditCoursePageModule extends BasePageModule {
    constructor(request) {
        super(request);
        this.initElements();
        this.initEvents();
        this.fire();
    }

    initElements() {
        this.textareaContainer = $('div#content');
        this.editor = this._createTextEditor();
        this.hiddenContent = $('textarea#hidden-content');
    }

    initEvents() {
        this._onWriteTextEditor();
    }

    fire() {
        this._injectContentInTextEditor();
    }

    _createTextEditor() {
        return TextEditorModule.factory(`#${this.textareaContainer.attr('id')}`, 'Contenu du cours ...');
    }

    _injectContentInTextEditor() {
        const content = this.hiddenContent.val();
        this.editor.setHtmlContent(content);
    }

    _onWriteTextEditor() {
        this.editor.on('editor-change', () => {
            const content = this.editor.getHtmlContent();
            this.hiddenContent.val(content);

        })
    }
}
