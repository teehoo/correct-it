import BasePageModule from "../BasePageModule";
import Parallax from "parallax-js";

export default class RegisterStudentPageModule extends BasePageModule {
    constructor(request) {
        super(request);
        this.initElements();
        this.initEvents();
        this.fire();
    }

    initElements() {

    }

    initEvents() {

    }

    fire() {
        this.handleParallax();
    }

    /**
     * Handle parallax
     * @return {void}
     */
    handleParallax() {
        const scene = this.DOM.studRegisterImageContainer[0];
        console.log(scene)
        let parallax = new Parallax(scene);
    }
}
