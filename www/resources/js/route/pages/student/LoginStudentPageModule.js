import BasePageModule from "../BasePageModule";
import Parallax from 'parallax-js';

export default class LoginStudentPageModule extends BasePageModule{
    constructor(request) {
        super(request);
        this.initElements();
        this.initEvents();
        this.fire();
    }

    initElements() {

    }

    initEvents() {

    }

    fire() {
        this.handleParallax();
    }

    /**
     * Handle parallax
     * @return {void}
     */
    handleParallax() {
        const scene = this.DOM.studLogImageContainer[0];
        let parallax = new Parallax(scene);
    }
}
