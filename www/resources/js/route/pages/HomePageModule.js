import BasePageModule from "./BasePageModule";
import Flickity from 'flickity';
import Rellax from 'rellax';

export default class HomePageModule extends BasePageModule{
    constructor(request) {
        super(request);
        this.initElements();
        this.initEvents();
        this.fire();
    }

    initElements() {
        this.homeCarousel = new Flickity(this.DOM.homeCarousel[0],
            {
                cellAlign: 'center',
                contain: true,
                freeScroll: false,
                pageDots: false,
                prevNextButtons: false,
                wrapAround: true,
                autoPlay: 4000
            });
    }

    initEvents() {
        this.onCLickCarouselArrowPrev();
        this.onCLickCarouselArrowNext();
    }

    fire() {
        this.handleParallax();
    }

    handleParallax() {
        let parallaxGreyShape = new Rellax(`.${this.DOM.homeCircleGreyShape.attr('class')}`);
        let parallaxRedShape = new Rellax(`.${this.DOM.homeCircleRedShape.attr('class')}`, {
            center: true
        });
    }

    /**
     * Go to the previous cell
     */
    onCLickCarouselArrowPrev() {
        this.DOM.homeCarouselArrowPrev.on('click', () => {
            this.homeCarousel.previous();
        })
    }

    /**
     * Go to the next cell
     */
    onCLickCarouselArrowNext() {
        this.DOM.homeCarouselArrowNext.on('click', () => {
            this.homeCarousel.next();
        })
    }
}
