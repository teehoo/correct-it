import domElements from "../../modules/domElements";
import PageService from "../../services/PageService";
import CommonPageModule from "./CommonPageModule";

export default class BasePageModule {
    constructor(request) {
        this.DOM = domElements;
        this.PageService = new PageService();
        this.request = request;
        if (!(this instanceof CommonPageModule)) {
            console.log(`Route actuelle: "${this.request.pathname}"`);
        }
    }

    // All classes extends of BasePageModule need to implements these methods :
    // initElements();
    // initEvents();
    // fire(request);

}
