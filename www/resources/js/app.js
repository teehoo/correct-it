// Import dependencies
require('./bootstrap');

import $ from 'jquery';

// Import custom dependencies
import AppRouter from "./route/route";

$(document).ready(() => {
    // Init router
    AppRouter.init();
});

